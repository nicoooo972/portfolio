
import {Particles} from "react-tsparticles";
import {loadFull} from "tsparticles";
import React, { useCallback } from "react";

const ParticlesContainer = (callback, deps) => {
  // init
  const particlesInit = useCallback(async (engine) =>{
    await loadFull(engine);
  }, []);

  const particlesLoaded = useCallback(async ()=> {}, []);

  return (
      <Particles className='w-full h-full absolute translate-z-0'
          id='tsparticles'
          init={particlesInit}
          loaded={particlesLoaded}
          options={{
            fullScreen: {enable: false},
            background: {
              color: {
                value: ''
              },
            },
            fpsLimit: 120,
            interactivity: {
              events: {
                onclick: {
                  enable: false,
                  mode: 'push',
                },
                  onHover: {
                    enable: true,
                      mode: 'repulse',
                  },
                  resize: true,
              },
                modes: {
                  quantity: 90,
                }
            },
          }}
      />
  );
};

export default ParticlesContainer;
